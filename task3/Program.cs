﻿using System;
using System.Collections.Generic;

namespace task
{
    class Program
    {
        static public bool IsOperator(string op)
        {
            if (op == "+" || op == "-" || op == "/" || op == "*")
                return true;
            return false;
        }

        static public int Operation(string op, int n1, int n2)
        {
            int result = 0;
            if (op == "+")
                result = n1 + n2;
            if (op == "-")
                result = n1 - n2;
            if (op == "*")
                result = n1 * n2;
            if (op == "/")
                result = n1 / n2;

            return result;
        }
        static void Main(string[] args)
        {
            string[] tokens = Console.ReadLine().Split(' ');

            Stack<string> stack = new Stack<string>();
            for(int i = 0; i < tokens.Length; i++)
            {
                stack.Push(tokens[i]);
                if (IsOperator(tokens[i]))
                {
                    string op = stack.Pop();
                    int n2 = Convert.ToInt32(stack.Pop());
                    int n1 = Convert.ToInt32(stack.Pop());
                    int result = Operation(op, n1, n2);
                    stack.Push(Convert.ToString(result));
                }
            }

            Console.WriteLine(stack.Pop());
        }
    }
}
