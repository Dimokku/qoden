﻿using System;
using System.Linq;

namespace task
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] words = Console.ReadLine().Split(' ');

            Text text = new Text();
            for (int i = 0; i<words.Length; i++)
                text.Add_word(words[i]);

            text.words.Sort(text.compare);
            Chart(text);
            Console.ReadLine();
        }

        //Draws a chart of the frequency of words
        public static void Chart(Text text)
        {
            int max_count = text.words.ElementAt(0).counter;
            int max_length = text.words.ElementAt(0).word.Length;

            for (int i = 0; i < text.words.Count(); i++)
            {
                int temp = text.words.ElementAt(i).counter;
                int length = text.words.ElementAt(i).word.Length;

                if (max_count < temp)
                    max_count = temp;

                if (max_length < length)
                    max_length = length;
            }

            for (int i = 0; i < text.words.Count(); i++)
            {
                if (text.words.ElementAt(i).word.Length < max_length)
                    for (int j = 0; j < max_length - text.words.ElementAt(i).word.Length; j++)
                        Console.Write("_");
                Console.Write($"{text.words.ElementAt(i).word} ");


                double dots = ((double)text.words.ElementAt(i).counter) / max_count * 10;

                if (dots - (int)dots >= 0.5) //round to whole
                    dots = (int)dots + 1;

                for (int j = 0; j < (int)dots; j++)
                    Console.Write(".");

                Console.WriteLine();
            }
        }
    }
}
