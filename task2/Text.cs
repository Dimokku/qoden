﻿using System.Collections.Generic;
using System.Linq;


namespace task
{
    class Text
    {
        public ListCompare compare = new ListCompare();
        public List<Word> words = new List<Word>();

        public class Word
        {
            public int counter = 0;
            public string word;

            public Word(string word)
            {
                this.word = word;
                counter = 1;
            }
        }

        public void Add_word(string word)
        {
            Word new_word = new Word(word);
            bool check = false;
            for (int i = 0; i < words.Count(); i++)
            {
                if (words.ElementAt(i).word == new_word.word)
                {
                    check = true;
                    break;
                }
            }

            if (words.Count() == 0 || !check)
                words.Add(new_word);
            else
            {
                for (int i = 0; i < words.Count(); i++)
                {
                    if (words.ElementAt(i).word == new_word.word)
                    {
                        words.ElementAt(i).counter++;
                        break;
                    }
                }
            }
        }

        public class ListCompare : IComparer<Word>
        {
            public int Compare(Word word1, Word word2)
            {
                if (word1.counter > word2.counter)
                    return 1;
                else if (word1.counter < word2.counter)
                    return -1;
                return 0;
            }
        }
    }
}

