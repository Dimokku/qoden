﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task
{
    class Program
    {
        static void Main(string[] args)
        {
            int N = Convert.ToInt32(Console.ReadLine());
            HashTable ht = new HashTable(N);

            string[] numbers = Console.ReadLine().Split(' ');
            for (int i = 0; i < numbers.Length; i++) { 
                ht.Insert(Convert.ToInt32(numbers[i]));
                }

            ht.Print();
        }
    }
}
