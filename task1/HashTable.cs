﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task
{
    class HashTable
    {     
        public Array<ListNode<int>>[] values;

        public void Print()
        {
            for(int i = 0; i < values.Length; i++)
            {
                Console.Write($"{i} : ");
                ListNode<int> node = values[i].head;
                while (node != null)
                {
                    Console.Write($"{node.get_val()} ");
                    node = node.get_next();
                }

                Console.WriteLine();
            }
        }
        public HashTable(int N)
        {
            values = new Array<ListNode<int>>[N];
            for (int i = 0; i < N; i++)
                values[i] = new Array<ListNode<int>>();
        }
        public void Insert(int newValue)
        {
            int hash = newValue % values.Length;
            values[hash].Add(newValue);
        }
    }

    public class Array<T>
    {
        public ListNode<int> head;
        public ListNode<int> tail;
        int size = 0;

        public void Add(int newValue)
        {
            if(size == 0)
            {
                head = new ListNode<int>(newValue);
                head.set_next(null);
                tail = head;
                tail.set_next(null);
            }
            else
            {
                ListNode<int> current = head;
                while (current.get_next() != null)
                    current = current.get_next();

                ListNode<int> temp = new ListNode<int>(newValue);
                temp.set_next(null);
                current.set_next(temp);
                tail = temp;
            }
            size++;
        }
    }

    public class ListNode<T>
    {
        T value;
        ListNode<T> next;
        public T get_val()
        {
            return value;
        }
        public void set_next(ListNode<T> next)
        {
            this.next = next;
        }
        public ListNode<T> get_next()
        {
            return this.next;
        }
        public ListNode(T value)
        {
            Insert(value);
        }
        public void Insert(T newValue)
        {
            value = newValue;
        }
    }
}
